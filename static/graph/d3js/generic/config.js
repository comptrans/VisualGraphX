// dependencies
define(['plugin/graph/d3js/common/config'], function(d3js_config) {
        return $.extend(true, {}, d3js_config, {
            title: 'Generic',
            category: 'Molecular Networks'
        });
});
